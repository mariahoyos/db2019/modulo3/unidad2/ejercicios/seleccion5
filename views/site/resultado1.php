<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\widgets\ListView;
?>

<div class="jumbotron">
  <h2><?= $enunciado ?></h2>

  <p><?= $sql ?></p>
</div>

<div class="row">
<?= ListView::widget([
            'dataProvider' => $resultados,
            //'itemView' => function ($model, $key, $index, $widget) use ($resultados) {
                //return $this->render('_resultado1',['model' => $model,'resultado'=>$resultados]);},
            'itemView' => '_resultado1',
            'layout'=>"{summary}\n{pager}\n{items}",
        ]);
?>
</div>