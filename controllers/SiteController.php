<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Ciclista;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\Lleva;
use app\models\Puerto;
use yii\widgets\ListView;
use app\models\Equipo;
use app\models\Etapa;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionCrud(){
        return $this->render("gestion");
    }
    
    public function actionConsulta1a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                    ->select("nombre, edad")
                    ->distinct()
                    ->joinWith('etapas', true)
                    ->where("etapa.dorsal IS NULL"),
                    
            
        ]);
        
        //var_dump(Ciclista::find()->with('etapas')->all()[2]->etapas); prueba para usar with en vez de joinWith
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Nombre y edad de los ciclistas que NO han ganado etapas",
            "sql"=>"SELECT DISTINCT nombre, edad FROM ciclista LEFT JOIN etapa ON ciclista.dorsal = etapa.dorsal WHERE etapa.dorsal IS NULL",
        ]);
        
    }
    public function actionConsulta1(){
        
        /*$numero = Yii::$app->db
                ->createCommand('select count(*)cuenta from ciclista')
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT nombre, edad FROM ciclista LEFT JOIN etapa ON ciclista.dorsal = etapa.dorsal WHERE etapa.dorsal IS NULL',
            'pagination'=>[
                'pageSize' => 6,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Nombre y edad de los ciclistas que NO han ganado etapas",
            "sql"=>"SELECT DISTINCT nombre, edad FROM ciclista LEFT JOIN etapa ON ciclista.dorsal = etapa.dorsal WHERE etapa.dorsal IS NULL",
        ]);
    }
    
    public function actionConsulta2a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                    ->select("nombre, edad")
                    ->distinct()
                    ->joinWith('puertos', true)
                    ->where("puerto.dorsal IS NULL"),
//            'pagination'=>[
//                'pageSize'=>1,
//            ]
        ]);
        
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad'],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"Nombre y edad de los ciclistas que NO han ganado puertos",
            "sql"=>"SELECT DISTINCT nombre, edad FROM  ciclista LEFT JOIN puerto ON ciclista.dorsal = puerto.dorsal WHERE puerto.dorsal IS NULL",
        ]);
        
    }
    
    public function actionConsulta2(){
        //mediante DAO
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(distinct edad) FROM ciclista WHERE nomequipo = 'Artiach'")
                ->queryScalar();*/
        
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT DISTINCT nombre, edad FROM  ciclista LEFT JOIN puerto ON ciclista.dorsal = puerto.dorsal WHERE puerto.dorsal IS NULL",
            //'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 6,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','edad'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Nombre y edad de los ciclistas que NO han ganado puertos",
            "sql"=>"SELECT DISTINCT nombre, edad FROM  ciclista LEFT JOIN puerto ON ciclista.dorsal = puerto.dorsal WHERE puerto.dorsal IS NULL",
        ]);
    }
    
    public function actionConsulta3a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Equipo::find()
                    ->select("director")
                    ->distinct()
                    ->joinWith('ciclistas', false)
                    ->leftJoin('etapa', 'ciclista.dorsal=etapa.dorsal')
                    ->where("etapa.dorsal IS NULL"),
            'pagination'=>[
                'pageSize'=>6,
            ]
        ]);
        
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['director'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Listar el director de los equipos que tengan ciclistas que NO hayan ganado ninguna etapa",
            "sql"=>"SELECT DISTINCT director FROM equipo LEFT JOIN ciclista LEFT JOIN etapa ON ciclista.dorsal = etapa.dorsal ON equipo.nomequipo = ciclista.nomequipo WHERE etapa.dorsal IS NULL",
        ]);
        
        /*echo ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => 'resultado1',
        ]);*/
        
    }
    
    public function actionConsulta3(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(DISTINCT edad) FROM ciclista WHERE nomequipo = 'Artiach' OR nomequipo = 'Amore Vita'")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT DISTINCT director FROM equipo LEFT JOIN ciclista LEFT JOIN etapa ON ciclista.dorsal = etapa.dorsal ON equipo.nomequipo = ciclista.nomequipo WHERE etapa.dorsal IS NULL",
            //'totalCount' => $numero,
            /*'pagination' =>[
                'pageSize'=>5,
            ]*/
        ]);
        
        return $this->render('resultado',[
            "resultados" => $dataProvider,
            "campos"=>['director'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Listar el director de los equipos que tengan ciclistas que NO hayan ganado ninguna etapa",
            "sql"=>"SELECT DISTINCT director FROM equipo LEFT JOIN ciclista LEFT JOIN etapa ON ciclista.dorsal = etapa.dorsal ON equipo.nomequipo = ciclista.nomequipo WHERE etapa.dorsal IS NULL",
        ]);
            
        
        
    }
    
    public function actionConsulta4a(){
        //mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                    ->select("ciclista.dorsal, nombre")
                    ->distinct()
                    ->joinWith('llevas', false)
                    ->where("lleva.dorsal IS NULL"),
            'pagination'=>[
                'pageSize'=>6,
            ]
        ]);
        
        
        //$a=Equipo::find()->joinWith('ciclistas',false,'inner join')->innerjoin('etapa', 'etapa.dorsal=ciclista.dorsal')->all();
        //var_dump($a[0]->ciclistas);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"Dorsal y nombre de los ciclistas que NO hayan llevado ningún maillot",
            "sql"=>"SELECT DISTINCT ciclista.dorsal, nombre FROM ciclista LEFT JOIN lleva ON ciclista.dorsal = lleva.dorsal WHERE lleva.dorsal IS NULL",
        ]);
        
    }
    
    public function actionConsulta4(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM ciclista WHERE edad<24 OR edad>30")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT DISTINCT ciclista.dorsal, nombre FROM ciclista LEFT JOIN lleva ON ciclista.dorsal = lleva.dorsal WHERE lleva.dorsal IS NULL",
            //'totalCount' =>$numero,
            'pagination'=>[
                'pageSize'=>5
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"Dorsal y nombre de los ciclistas que NO hayan llevado ningún maillot",
            "sql"=>"SELECT DISTINCT ciclista.dorsal, nombre FROM ciclista LEFT JOIN lleva ON ciclista.dorsal = lleva.dorsal WHERE lleva.dorsal IS NULL",
        ]);
    }
    
    public function actionConsulta5a(){
        //mediante active record
        
        $subconsulta = Lleva::find()
                    ->select("dorsal")
                    ->distinct()
                    ->innerJoin('maillot','lleva.código=maillot.código')
                    ->where("color='amarillo'");
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                    ->select("ciclista.dorsal, nombre")
                    ->distinct()
                    ->leftJoin(["c1"=>$subconsulta], 'c1.dorsal=ciclista.dorsal')
                    ->where("c1.dorsal IS NULL"),
            'pagination'=>[
                'pageSize'=>6,
            ]
        ]);
        
        
        
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>"Dorsal y nombre de los ciclistas que NO hayan llevado el maillot amarillo NUNCA",
            "sql"=>"SELECT ciclista.dorsal, nombre FROM ciclista LEFT JOIN (SELECT  DISTINCT dorsal FROM lleva JOIN maillot ON lleva.código = maillot.código WHERE color='amarillo')c1 ON ciclista.dorsal = c1.dorsal WHERE c1.dorsal IS NULL",
        ]);
         
    }
    
    public function actionConsulta5(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM ciclista WHERE (edad BETWEEN 28 AND 32) AND (nomequipo = 'Banesto')")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT ciclista.dorsal, nombre FROM ciclista LEFT JOIN (SELECT  DISTINCT dorsal FROM lleva JOIN maillot ON lleva.código = maillot.código WHERE color='amarillo')c1 ON ciclista.dorsal = c1.dorsal WHERE c1.dorsal IS NULL",
            //'totalCount' =>$numero,
            'pagination'=>[
                'pageSize'=>6
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado"=>"Dorsal y nombre de los ciclistas que NO hayan llevado el maillot amarillo NUNCA",
            "sql"=>"SELECT ciclista.dorsal, nombre FROM ciclista LEFT JOIN (SELECT  DISTINCT dorsal FROM lleva JOIN maillot ON lleva.código = maillot.código WHERE color='amarillo')c1 ON ciclista.dorsal = c1.dorsal WHERE c1.dorsal IS NULL",
        ]);
    }
    
    public function actionConsulta6a(){
    //por terminar    
        $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find()
                    ->select("etapa.numetapa")
                    ->distinct()
                    ->joinWith('puertos', false)
                    ->where("puerto.numetapa IS NULL"),
            'pagination'=>[
                'pageSize'=>6,
            ]
        ]);
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['numetapa'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>"Indicar el numetapa de las etapas que NO tengan puerto",
            "sql"=>"SELECT etapa.numetapa FROM etapa LEFT JOIN puerto ON etapa.numetapa = puerto.numetapa WHERE puerto.numetapa IS NULL",
        ]);
        
    }
    
    public function actionConsulta6(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(distinct nombre) FROM  ciclista WHERE CHAR_LENGTH(nombre)>8")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT etapa.numetapa FROM etapa LEFT JOIN puerto ON etapa.numetapa = puerto.numetapa WHERE puerto.numetapa IS NULL",
           //'totalCount' => $numero,
           'pagination'=>[
               'pageSize'=>6,
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['numetapa'],
            "titulo"=>"Consulta 6 con DAO",
            "enunciado"=>"Indicar el numetapa de las etapas que NO tengan puerto",
            "sql"=>"SELECT etapa.numetapa FROM etapa LEFT JOIN puerto ON etapa.numetapa = puerto.numetapa WHERE puerto.numetapa IS NULL",
        ]);
        
    }
    
    public function actionConsulta7a(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find()
                    ->select("avg(kms)media")
                    ->joinWith('puertos', false)
                    ->where("puerto.numetapa IS NULL"),
//            'pagination'=>[
//                'pageSize'=>6,
//            ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['media'],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>" Indicar la distancia media de las etapas que NO tengan puertos",
            "sql"=>"SELECT AVG(kms) FROM etapa LEFT JOIN puerto ON etapa.numetapa = puerto.numetapa WHERE puerto.numetapa IS NULL",
            
        ]);
     
    }
    
    public function actionConsulta7(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM ciclista")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT AVG(kms)media FROM etapa LEFT JOIN puerto ON etapa.numetapa = puerto.numetapa WHERE puerto.numetapa IS NULL",
           //'totalCount' => $numero,
           'pagination' => [
               'pageSize' =>6
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['media'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"Indicar la distancia media de las etapas que NO tengan puertos",
            "sql"=>"SELECT AVG(kms) FROM etapa LEFT JOIN puerto ON etapa.numetapa = puerto.numetapa WHERE puerto.numetapa IS NULL",
        ]);
        
    }
    
    public function actionConsulta8a(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                    ->distinct()
                    ->select("count(ciclista.dorsal)dorsal")
                    ->joinWith('etapas', false)
                    ->where("etapa.dorsal IS NULL"),
            'pagination'=>[
                'pageSize'=>6,
            ]
        ]);
        
        //var_dump($dataProvider);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=>"Listar el número de ciclistas que NO hayan ganado alguna etapa",
            "sql"=>"SELECT COUNT(ciclista.dorsal)dorsal FROM ciclista LEFT JOIN etapa ON ciclista.dorsal = etapa.dorsal WHERE etapa.dorsal IS NULL",
            
        ]);
     
    }
    
    public function actionConsulta8(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM lleva WHERE código = 'MGE'")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT COUNT(ciclista.dorsal)dorsal FROM ciclista LEFT JOIN etapa ON ciclista.dorsal = etapa.dorsal WHERE etapa.dorsal IS NULL",
           //'totalCount' => $numero,
           'pagination' => [
               'pageSize' =>5
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"Listar el número de ciclistas que NO hayan ganado alguna etapa",
            "sql"=>"SELECT COUNT(ciclista.dorsal)dorsal FROM ciclista LEFT JOIN etapa ON ciclista.dorsal = etapa.dorsal WHERE etapa.dorsal IS NULL",
        ]);
        
    }
    
    public function actionConsulta9a(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find()
                    ->select("etapa.dorsal")
                    ->distinct()
                    ->joinWith('puertos', false)
                    ->where("puerto.numetapa IS NULL"),
            'pagination'=>[
                'pageSize'=>6,
            ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 9 con Active Record",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado alguna etapa que NO tenga puerto",
            "sql"=>"SELECT DISTINCT etapa.dorsal FROM etapa LEFT JOIN puerto ON etapa.numetapa = puerto.numetapa WHERE puerto.numetapa IS NULL",
            
        ]);
     
    }
    
    public function actionConsulta9(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM puerto")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT DISTINCT etapa.dorsal FROM etapa LEFT JOIN puerto ON etapa.numetapa = puerto.numetapa WHERE puerto.numetapa IS NULL",
           //'totalCount' => $numero,
           'pagination' => [
               'pageSize' =>6
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado alguna etapa que NO tenga puerto",
            "sql"=>"SELECT DISTINCT etapa.dorsal FROM etapa LEFT JOIN puerto ON etapa.numetapa = puerto.numetapa WHERE puerto.numetapa IS NULL",
        ]);
        
    }
    
    public function actionConsulta10a(){
        
        $subconsulta = Etapa::find()
                    ->select("etapa.dorsal")
                    ->distinct()
                    ->innerJoinWith('puertos',true);
        
        
        
        $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find()
                    ->select("etapa.dorsal")
                    ->distinct()
                    ->leftJoin(["c1"=>$subconsulta], 'c1.dorsal=etapa.dorsal')
                    ->where("c1.dorsal IS NULL"),
            'pagination'=>[
                'pageSize'=>6,
            ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 10 con Active Record",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado únicamente etapas que no tengan puertos",
            "sql"=>"SELECT DISTINCT etapa.dorsal FROM etapa LEFT JOIN (SELECT DISTINCT etapa.dorsal FROM etapa JOIN puerto ON etapa.numetapa = puerto.numetapa)c1 ON etapa.dorsal=c1.dorsal WHERE c1.dorsal is NULL",
            
        ]);
     
    }
    
    public function actionConsulta10(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM puerto WHERE pendiente>8 OR altura BETWEEN 1800 AND 3000")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT DISTINCT etapa.dorsal FROM etapa LEFT JOIN (SELECT DISTINCT etapa.dorsal FROM etapa JOIN puerto ON etapa.numetapa = puerto.numetapa)c1 ON etapa.dorsal=c1.dorsal WHERE c1.dorsal is NULL",
           //'totalCount' => $numero,
           'pagination' => [
               'pageSize' =>5
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado únicamente etapas que no tengan puertos",
            "sql"=>"SELECT DISTINCT etapa.dorsal FROM etapa LEFT JOIN (SELECT DISTINCT etapa.dorsal FROM etapa JOIN puerto ON etapa.numetapa = puerto.numetapa)c1 ON etapa.dorsal=c1.dorsal WHERE c1.dorsal is NULL",
        ]);
        
    }
    
    public function actionConsulta11a(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select('nombre')
                ->distinct()
                ->innerJoinWith('puertos', true)
                ->where("nomequipo='Banesto'")
                
                
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 11 con Active Record",
            "enunciado"=>"Indicar el nombre de los puertos que hayan sido ganados por ciclistas de Banesto",
            "sql"=>"SELECT DISTINCT nombre FROM ciclista JOIN puerto ON ciclista.dorsal = puerto.dorsal WHERE nomequipo='Banesto'",
            
        ]);
     
    }
    
    public function actionConsulta11(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT DISTINCT nombre FROM ciclista JOIN puerto ON ciclista.dorsal = puerto.dorsal WHERE nomequipo='Banesto'",
           //'totalCount' => $numero,
           'pagination' => [
               'pageSize' =>5
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 11 con DAO",
            "enunciado"=>"Indicar el nombre de los puertos que hayan sido ganados por ciclistas de Banesto",
            "sql"=>"SELECT DISTINCT nombre FROM ciclista JOIN puerto ON ciclista.dorsal = puerto.dorsal WHERE nomequipo='Banesto'",
        ]);
        
    }
    
    public function actionConsulta12a(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find()
                ->select('etapa.numetapa')
                ->distinct()
                ->innerJoinWith('dorsal0')
                ->innerJoinWith('puertos')
                ->where("nomequipo='Banesto'")
                ->andWhere("kms>=200")
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['numetapa'],
            "titulo"=>"Consulta 12 con Active Record",
            "enunciado"=>"Listar el número de las etapas que tengan puerto que hayan sido ganadas por ciclistas de Banesto con más de 200km o 200km",
            "sql"=>"SELECT DISTINCT etapa.numetapa FROM etapa JOIN ciclista ON etapa.dorsal = ciclista.dorsal JOIN puerto ON etapa.numetapa = puerto.numetapa WHERE nomequipo='Banesto' AND kms>=200",
            
        ]);
     
    }
    
    public function actionConsulta12(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT DISTINCT etapa.numetapa FROM etapa JOIN ciclista ON etapa.dorsal = ciclista.dorsal JOIN puerto ON etapa.numetapa = puerto.numetapa WHERE nomequipo='Banesto' AND kms>=200",
           //'totalCount' => $numero,
           'pagination' => [
               'pageSize' =>1
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['numetapa'],
            "titulo"=>"Consulta 12 con DAO",
            "enunciado"=>"Listar el número de las etapas que tengan puerto que hayan sido ganadas por ciclistas de Banesto con más de 200km o 200km",
            "sql"=>"SELECT DISTINCT etapa.numetapa FROM etapa JOIN ciclista ON etapa.dorsal = ciclista.dorsal JOIN puerto ON etapa.numetapa = puerto.numetapa WHERE nomequipo='Banesto' AND kms>=200",
        ]);
        
    }
    
    public function actionConsulta14a(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()
                ->select("avg(altura)media")
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['media'],
            "titulo"=>"Consulta 14 con Active Record",
            "enunciado"=>"Indicar la altura media de los puertos",
            "sql"=>"SELECT AVG(altura) FROM puerto",
            
        ]);
     
    }
    
    public function actionConsulta14(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT AVG(altura)media FROM puerto",
           //'totalCount' => $numero,
           'pagination' => [
               'pageSize' =>1
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['media'],
            "titulo"=>"Consulta 14 con DAO",
            "enunciado"=>"Indicar la altura media de los puertos",
            "sql"=>"SELECT AVG(altura) FROM puerto",
        ]);
        
    }
    
    public function actionConsulta18a(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Lleva::find()
                ->select("dorsal, código, count(*)cuenta")
                ->groupby("dorsal,código"),
            'pagination' => [
               'pageSize' =>5
           ]
        ]);
        
        return $this->render('resultado1', [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','código','cuenta'],
            "titulo"=>"Consulta 18 con Active Record",
            "enunciado"=>"Listar el dorsal del ciclista con el código de maillot y cuántas veces ese ciclista ha llevado ese maillot",
            "sql"=>"SELECT dorsal, código, COUNT(*) FROM lleva GROUP BY dorsal, código",
            
        ]);
        
     
    }
    
    public function actionConsulta18(){
        
        /*$numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000")
                ->queryScalar();*/
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT dorsal, código, COUNT(*)cuenta FROM lleva GROUP BY dorsal, código",
           //'totalCount' => $numero,
           'pagination' => [
               'pageSize' =>5
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','código','cuenta'],
            "titulo"=>"Consulta 18 con DAO",
            "enunciado"=>"Listar el dorsal del ciclista con el código de maillot y cuántas veces ese ciclista ha llevado ese maillot",
            "sql"=>"SELECT dorsal, código, COUNT(*) FROM lleva GROUP BY dorsal, código",
        ]);
        
    }
    
}
