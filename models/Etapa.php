<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "etapa".
 *
 * @property int $numetapa
 * @property int $kms
 * @property string $salida
 * @property string $llegada
 * @property int $dorsal
 *
 * @property Ciclista $dorsal0
 * @property Lleva[] $llevas
 * @property Maillot[] $códigos
 * @property Puerto[] $puertos
 */
class Etapa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    
    public $cuenta;
    public $media;
    
    public static function tableName()
    {
        return 'etapa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numetapa', 'kms', 'salida', 'llegada'], 'required'],
            [['numetapa', 'kms', 'dorsal'], 'integer'],
            [['salida', 'llegada'], 'string', 'max' => 35],
            [['numetapa'], 'unique'],
            [['dorsal'], 'exist', 'skipOnError' => true, 'targetClass' => Ciclista::className(), 'targetAttribute' => ['dorsal' => 'dorsal']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'numetapa' => 'Numetapa',
            'kms' => 'Kms',
            'salida' => 'Salida',
            'llegada' => 'Llegada',
            'dorsal' => 'Dorsal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDorsal0()
    {
        return $this->hasOne(Ciclista::className(), ['dorsal' => 'dorsal']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLlevas()
    {
        return $this->hasMany(Lleva::className(), ['numetapa' => 'numetapa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCódigos()
    {
        return $this->hasMany(Maillot::className(), ['código' => 'código'])->viaTable('lleva', ['numetapa' => 'numetapa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPuertos()
    {
        return $this->hasMany(Puerto::className(), ['numetapa' => 'numetapa']);
    }
}
